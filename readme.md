# HACKATHON - Deploy Server Configuration
## Windows machine

###The supported version is windows server 2012 R2.

- Open *powershell ISE* from start Menu -> all application (by using the normal powershell version you could have some problem) and run the following command

        (new-object Net.WebClient).DownloadString("https://s3-eu-west-1.amazonaws.com/scripts-hackathon/deploy-win.ps1") | iex
    
- Follow the configuration process
- *IMPORTANT* close the browser after the installation of Couchbase to let the script continue
- If you restart the machine you will have to manually restart the syncgateway process


## Linux machine

###The supported version is Ubuntu 14.04 LTS

- From the terminal run the following command

        wget 'https://s3-eu-west-1.amazonaws.com/scripts-hackathon/deploy.sh' && chmod +x deploy.sh && ./deploy.sh
        

# AZURE vm provisioning

### We suggest to provision the machine without Network security group for speeding up the process, if you prefer instead you can use the network SG and open the following ports

- Couchbase ports http://developer.couchbase.com/documentation/server/current/install/install-ports.html

- Syncgateway ports http://developer.couchbase.com/documentation/mobile/current/develop/guides/sync-gateway/administering-sync-gateway/administering-the-rest-apis/index.html

- Additional ports (80, 443, ecc...)
