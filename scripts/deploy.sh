#!/bin/bash

sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update
sudo apt-get install git -y
sudo apt-get install ansible -y

git clone https://g_cismondi@bitbucket.org/spindox_cloud_factory/ansible-node-couchbase.git

ansible-playbook ansible-node-couchbase/deploy.yml

rm -rf ansible-node-couchbase

echo "### SETUP COMPLETED ###"