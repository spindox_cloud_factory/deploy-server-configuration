Write-Output "Downloading node..."
$url = "https://nodejs.org/dist/v6.2.0/node-v6.2.0-x64.msi"
$output = "$PSScriptRoot\node.msi"
$start_time = Get-Date
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s) to download node"
Write-Output "installing node..."
$setup=Start-Process "$PSScriptRoot\node.msi" -ArgumentList "/passive" -Wait
Write-Output "done!"


Write-Output "Downloading net framework ..."
$url = "https://download.microsoft.com/download/1/B/E/1BE39E79-7E39-46A3-96FF-047F95396215/dotNetFx40_Full_setup.exe"
$output = "$PSScriptRoot\NET.exe"
$start_time = Get-Date
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s) to download net framework 4"
Write-Output "installing net framework..."
$setup=Start-Process "$PSScriptRoot\NET.exe " -ArgumentList "/q /norestart /ChainingPackage ADMINDEPLOYMENT" -Wait
Write-Output "done!"

Write-Output "Configuring IIS"
DISM.EXE /enable-feature /all /online /featureName:IIS-WebServerRole /featureName:IIS-WebServer /featureName:IIS-CommonHttpFeatures /featureName:IIS-StaticContent /featureName:IIS-DefaultDocument /featureName:IIS-DirectoryBrowsing /featureName:IIS-HttpErrors /featureName:IIS-HttpRedirect /featureName:IIS-ApplicationDevelopment /featureName:IIS-ASPNET /featureName:IIS-NetFxExtensibility /featureName:IIS-ASPNET45 /featureName:IIS-NetFxExtensibility45 /featureName:IIS-ASP /featureName:IIS-CGI /featureName:IIS-ISAPIExtensions /featureName:IIS-ISAPIFilter /featureName:IIS-ServerSideIncludes /featureName:IIS-HealthAndDiagnostics /featureName:IIS-HttpLogging /featureName:IIS-LoggingLibraries /featureName:IIS-RequestMonitor /featureName:IIS-HttpTracing /featureName:IIS-CustomLogging /featureName:IIS-ODBCLogging /featureName:IIS-Security /featureName:IIS-BasicAuthentication /featureName:IIS-WindowsAuthentication /featureName:IIS-DigestAuthentication /featureName:IIS-ClientCertificateMappingAuthentication /featureName:IIS-IISCertificateMappingAuthentication /featureName:IIS-URLAuthorization /featureName:IIS-RequestFiltering /featureName:IIS-IPSecurity /featureName:IIS-Performance /featureName:IIS-HttpCompressionStatic /featureName:IIS-HttpCompressionDynamic /featureName:IIS-WebDAV /featureName:IIS-WebServerManagementTools /featureName:IIS-ManagementScriptingTools /featureName:IIS-ManagementService /featureName:IIS-IIS6ManagementCompatibility /featureName:IIS-Metabase /featureName:IIS-WMICompatibility /featureName:IIS-LegacyScripts /featureName:IIS-FTPServer /featureName:IIS-FTPSvc /featureName:IIS-FTPExtensibility /featureName:NetFx4Extended-ASPNET45 /featureName:IIS-ApplicationInit /featureName:IIS-WebSockets /featureName:IIS-CertProvider /featureName:IIS-ManagementConsole /featureName:IIS-LegacySnapIn
Write-Output "done!"

Write-Output "Downloading couchbase..."
$url = "http://packages.couchbase.com/releases/4.0.0/couchbase-server-community_4.0.0-windows_amd64.exe"
$output = "$PSScriptRoot\couchbase.exe"
$start_time = Get-Date
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s) to download couchbase"
Write-Output "installing net couchbase..."
$setup=Start-Process "$PSScriptRoot\couchbase.exe"  -Wait
Write-Output "done!"

Write-Output "Downloading sync-gateway..."
$url = "http://packages.couchbase.com/releases/couchbase-sync-gateway/1.1.1/couchbase-sync-gateway-community_1.1.1-10_x86_64.exe"
$output = "$PSScriptRoot\couchbasesg.exe"
$start_time = Get-Date
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s) to download sync-gateway"
Write-Output "installing net sync-gateway..."
$setup=Start-Process "$PSScriptRoot\couchbasesg.exe" -Wait
Write-Output "done!"

Write-Output "Configuring firewall..."
netsh advfirewall set allprofiles state off
Write-Output "done!"

Write-Output "Downloading Firefox..."
$url = "https://download.mozilla.org/?product=firefox-46.0.1-SSL&os=win64&lang=en-GB"
$output = "$Env:userprofile\Desktop\firefox.exe"
$start_time = Get-Date
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($url, $output)
Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s) to download firefox"
Write-Output "Firefox installer saved to Desktop"

Write-Output "starting sync_gateway..."
Start-Process "C:\Program Files (x86)\Couchbase\sync_gateway.exe"

Write-Output "EVERYTHING DONE, YOU'RE READY TO START!!!"


